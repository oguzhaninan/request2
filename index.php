<?php

$willRemoveKeys = [
    "Type", "CustomFilters", "AttributesSpecialization", "Recommended", "Score", "Distance", "IsMobileAdvertiser", "IsVriskoAdvertiser",
    "IsPremiumAdvertiser", "AttributesText", "Address_Street", "Address_PostCode", "Address_Region", "Address_Prefecture", "LogoPath", "StreetNo"
];

$proxiesPath = __DIR__.'/Proxies.3.json';
$cityPath = __DIR__.'/City.3.json';
$cities  = json_decode(file_get_contents($cityPath), true);
$proxies = json_decode(file_get_contents($proxiesPath), true);

$categories = json_decode(file_get_contents(__DIR__.'/DiffCats.json'), true);

function proxy($ip, $port) {
    return ['socks4' => [
        'proxy' => 'socks4://'.$ip.':'.$port,
        'request_fulluri' => true
    ]];
}

$url = "http://mobileservices.bulurum.com/json/ClassicSearch?UniqueID=b3bfd8ae2a40a295";

$catSecond = 100000;  // 1 second 1.000.000
$districtSecond = 5;
$citySecond = 5;

$_prox = array_pop($proxies);
$cxContext = stream_context_create(proxy($_prox['ip'], $_prox['port']));

foreach ($cities as $city) {
    foreach ($city['districts'] as $district) {
        $all = [];
        foreach ($categories as $cat) {
            $encodedUrl = $url.'&What='.rawurlencode($cat).'&Where='.rawurlencode($city['city'].', '.$district).'&PosX=26.00&PosY=41.98';
            $data = json_decode(file_get_contents($encodedUrl, false, $cxContext), true);
            $items = $data['ItemsContainer']['Items'];
            
            echo $city['city'].', '.$district.' => '.$cat. " Item Count: ". count($items) ."\n";

            $status = $data['Status']['Success'];
            if ($status) {
                for ($i=0; $i < count($items); $i++) {
                    foreach ($willRemoveKeys as $key) {
                        unset($items[$i][$key]);
                    }
                    $items[$i]['PosX'] = floatval($items[$i]['PosX']);
                    $items[$i]['PosY'] = floatval($items[$i]['PosY']);
                }

                $all = array_merge($all, $items);

                // echo "Waiting for ".$catSecond." seconds...\n";
                // usleep($catSecond);
            } else {
                if (!empty($proxies)) {
                    $_prox = array_pop($proxies);
                    $cxContext = stream_context_create(proxy($_prox['ip'], $_prox['port']));
                    echo "Proxy changed: ".$_prox['ip'].":".$_prox['port'].". Proxy count: ".count($proxies)."\n";
                    file_put_contents($proxiesPath, json_encode($proxies));
                } else {
                    echo "Proxy not found. ". $cat. " " . $city . "\n";
                    $dirPath = __DIR__.'/data/'.$city['city'];
                    if (! file_exists($dirPath)) {
                        mkdir($dirPath);
                    }
                    file_put_contents($dirPath.'/'.$district.'.json', json_encode($all));
                    exit();
                }
            }
        }

        $dirPath = __DIR__.'/data/'.$city['city'];
        if (! file_exists($dirPath)) {
            mkdir($dirPath);
        }
        file_put_contents($dirPath.'/'.$district.'.json', json_encode($all));

        echo $city['city'].", ". $district ." Completed. Total item count: ".count($all)." Waiting for ".$districtSecond." seconds...\n";
        sleep($districtSecond);
    }
    echo $city['city']." Completed. Waiting for ".$citySecond." seconds...\n";
    sleep($citySecond);
}
